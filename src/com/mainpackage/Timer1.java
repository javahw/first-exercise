package com.mainpackage;


public class Timer1 extends Thread {
    Counter counter;
    public Timer1(Counter c){
        this.counter = c;
    }

    public void run(){
        synchronized (counter){
            while(true){
                try {
                    counter.wait();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                int t = this.counter.getTime();
                if(t % 5 != 0){
                    System.out.println(t + " sec by Timer1");
                }
            }
        }
    }
}
