package com.mainpackage;

public class Counter extends Thread{
    private int time = 0;
    private boolean locked = false;

    public void run(){
        while (true){
            try {
                sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            setTime(this.time + 1);
            System.out.print("time: ");
        }
    }

    public synchronized int getTime(){
        while(locked){
            try {
                wait();
            }catch (InterruptedException e){  }
        }
        return time;
    }

    public synchronized void setTime(int t){
        locked = true;
        this.time = t;
        locked = false;
        notifyAll();
    }
}