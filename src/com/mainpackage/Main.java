package com.mainpackage;

public class Main {
    public static synchronized void main(String[] args){
        Counter counter = new Counter();
        counter.start();

        Timer1 timer1 = new Timer1(counter);
        timer1.start();

        Timer5 timer5 = new Timer5(counter);
        timer5.start();
    }
}
